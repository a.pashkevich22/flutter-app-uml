import 'package:uml_payment_app/services/token.service.dart';
import 'package:dio/dio.dart';
import 'package:uml_payment_app/constants.dart';
import 'package:uml_payment_app/models/payment/PaymentModel.dart';

Dio _http = new Dio();

class PaymentRepository {

	Future<List<PaymentModel>> fetchAll() async {
		TokenService tkn = new TokenService();
		String token = await tkn.getToken();

		if(token == null)
			return null;

		try {
			Response response = await _http.get('${Constant.serverUrl}/payment/',
					options: Options(
						headers: {
							"Authorization": 'Bearer $token'
						}
					)
			);

			if(response.data['ok']) {
				List data = List.from(response.data['payments']??[]);
				List<PaymentModel> result = [];
				data.forEach((value) => result.add(
						new PaymentModel(
							amount: value['amount'],
							forward: value['recipient'],
							id: value['id'],
							isAuthorized: value['status'],
							message: value['message']
						)
				));

				return result;
			} else
				return null;
		} on DioError catch(e) {
			return null;
		}
	}
}