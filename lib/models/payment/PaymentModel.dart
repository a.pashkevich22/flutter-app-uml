import 'package:uml_payment_app/services/token.service.dart';
import 'package:dio/dio.dart';
import 'package:uml_payment_app/constants.dart';

Dio _http = new Dio();

class PaymentModel {
	PaymentModel({
		this.amount,
		this.message,
		this.forward,
		this.id,
		this.isAuthorized
	}):assert(amount != null);

	final int id;
	final bool isAuthorized;
	final double amount;
	final String message;
	final String forward;



	Future<bool> authorize() async {
		if(this.isAuthorized??false)
			return true;

		TokenService tkn = new TokenService();
		String token = await tkn.getToken();

		if(token == null)
			return false;

		try {
			Response response = await _http.put(
					'${Constant.serverUrl}/payment/${this.id}',
					options: Options(
							headers: {
								"Authorization": 'Bearer $token'
							}
					)
			);

			if(response.data['ok'])
				return true;
			else
				return false;
		} on DioError catch(e) {
			return false;
		}

	}


}