import 'package:uml_payment_app/services/token.service.dart';
import 'package:dio/dio.dart';
import 'package:uml_payment_app/constants.dart';
import 'package:uml_payment_app/models/card/CardModel.dart';

Dio _http = new Dio();

class CardRepository {

	Future<List<CardModel>> fetchAll() async {
		TokenService tkn = new TokenService();
		String token = await tkn.getToken();

		if(token == null)
			return null;

		try {
			Response response = await _http.get('${Constant.serverUrl}/cards/',
					options: Options(
							headers: {
								"Authorization": 'Bearer $token'
							}
					)
			);

			if(response.data['ok']) {
				List data = List.from(response.data['cards']??[]);
				List<CardModel> result = [];
				data.forEach((value) => result.add(
						new CardModel(
								expiryMonth: '${value['expiry_month']}',
								expiryYear: '${value['expiry_year']}',
								id: value['id'],
								cvv: '${value['cvv']}',
								cardNumber: value['card_number']
						)
				));

				return result;
			} else
				return null;
		} on DioError catch(e) {
			return null;
		}
	}
}