import 'package:uml_payment_app/services/token.service.dart';
import 'package:dio/dio.dart';
import 'package:uml_payment_app/constants.dart';

Dio _http = new Dio();

class CardModel {
	CardModel({
		String cardNumber,
		this.expiryMonth,
		this.expiryYear,
		this.cvv,
		this.id
	}):_cardNumber = cardNumber;

	final int id;
	final String expiryMonth;
	final String _cardNumber;
	final String expiryYear;
	final String cvv;

	String get cardNumber {
		return '**** **** **** ${_cardNumber.substring(_cardNumber.length - 4,_cardNumber.length)} ';
	}



	Future<bool> remove() async {

		TokenService tkn = new TokenService();
		String token = await tkn.getToken();

		if(token == null)
			return false;

		try {
			Response response = await _http.delete(
					'${Constant.serverUrl}/cards/${this.id}',
					options: Options(
							headers: {
								"Authorization": 'Bearer $token'
							}
					)
			);

			if(response.data['ok'])
				return true;
			else
				return false;
		} on DioError catch(e) {
			return false;
		}

	}

	Future<bool> create() async {

		TokenService tkn = new TokenService();
		String token = await tkn.getToken();

		if(token == null)
			return false;

		try {
			Response response = await _http.post(
					'${Constant.serverUrl}/cards/',
					options: Options(
							headers: {
								"Authorization": 'Bearer $token'
							}
					),
					data: {
						'card_number': this._cardNumber,
						'cvv': this.cvv,
						'expiry_year': this.expiryYear,
						'expiry_month': this.expiryMonth
					}
			);

			if(response.data['ok'])
				return true;
			else
				return false;
		} on DioError catch(e) {
			return false;
		}

	}

}