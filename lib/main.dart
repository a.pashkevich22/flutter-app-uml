import 'package:flutter/material.dart';
import 'package:uml_payment_app/bfa.dart';
import 'package:flutter_stetho/flutter_stetho.dart';

void main(){
  Stetho.initialize();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'UMLProject',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BFA(),
    );
  }
}
