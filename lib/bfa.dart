import 'package:flutter/material.dart';
import 'package:uml_payment_app/services/token.service.dart';
import 'package:uml_payment_app/screens/Login.dart';
import 'package:uml_payment_app/screens/PaymentList.dart';

class BFA extends StatefulWidget {
  @override
  _BFAState createState() => _BFAState();
}

class _BFAState extends State<BFA> {

	void nextPageDelegate() async{
		try {
			String token = await TokenService().getToken();
			if(token != null)
				Navigator.of(context).pushReplacement(
						MaterialPageRoute(builder: (ctx) => PaymentListScreen())
				);
			else
				Navigator.of(context).pushReplacement(
						MaterialPageRoute(builder: (ctx) => LoginScreen())
				);
		} catch(e){
			Navigator.of(context).pushReplacement(
				MaterialPageRoute(builder: (ctx) => LoginScreen())
			);
		}
	}

	@override
  void initState() {
    nextPageDelegate();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
