import 'package:flutter/material.dart';
import 'package:uml_payment_app/models/payment/PaymentModel.dart';
import 'package:uml_payment_app/services/biometrics.service.dart';
class PaymentSingleScreen extends StatefulWidget {
  PaymentSingleScreen({this.payment});

  final PaymentModel payment;

  @override
  _PaymentSingleScreenState createState() => _PaymentSingleScreenState();
}

class _PaymentSingleScreenState extends State<PaymentSingleScreen> {
  bool canCheckBiometrics = false;
  BiometricsService bs = new BiometricsService();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  void initBiometrics() async {
    bool _canCheckBiometrics = await bs.canCheckBiometrics();

    setState(() {
      canCheckBiometrics = _canCheckBiometrics;
    });
  }

  void authorizePayment() async {
    if (!canCheckBiometrics) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Your device doesn\'t support biometrics')));
      return;
    }



    bool isAuthorized = await bs.authorize('To confirm the payment, use TouchID');
    if(isAuthorized){
      bool isSuccess = await widget.payment.authorize();
      if(isSuccess){
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Payment authorized successfully')));
        Navigator.of(context).pop();
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Error while authorizing payment')));
      }
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text('Seems like you\'re not a holder')));
    }
  }


  @override
  void initState() {
    initBiometrics();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        elevation: 0.0,
        title: Text("Payment"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 8, right: 8, top: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: Icon(
                  Icons.monetization_on,
                  size: 64.0,
                ),
              ),
              Container(
                height: 24.0,
              ),
              titsubtitWidget(title: 'Payment to:', subtitle: widget.payment.forward),
              titsubtitWidget(title: 'Amount:', subtitle: '\$ ${widget.payment.amount}'),
              titsubtitWidget(title: 'Payment message:', subtitle: widget.payment.message),
              widget.payment.isAuthorized ?? false
                  ? Container()
                  : Container(
                      width: double.infinity,
                      height: 56.0,
                      margin: EdgeInsets.symmetric(vertical: 24.0),
                      child: RaisedButton(
                        onPressed: authorizePayment,
                        elevation: 1.0,
                        color: Colors.blueAccent,
                        child: Text(
                          "Authorize payment",
                          style: TextStyle(fontWeight: FontWeight.w300, color: Colors.white),
                        ),
                      ),
                    )
            ],
          ),
        ),
      ),
    );
  }
}

Widget titsubtitWidget({String title, String subtitle}) {
  return Container(
    margin: EdgeInsets.symmetric(vertical: 12),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title ?? '',
          style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.w100),
        ),
        Text(
          subtitle ?? '',
          style: TextStyle(fontSize: 24.0, color: Colors.black, fontWeight: FontWeight.w300),
        ),
      ],
    ),
  );
}
