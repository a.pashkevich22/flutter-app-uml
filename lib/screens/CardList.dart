import 'package:flutter/material.dart';
import 'package:uml_payment_app/models/card/CardRepository.dart';
import 'package:uml_payment_app/models/card/CardModel.dart';
import 'package:uml_payment_app/screens/CardCreate.dart';
import 'package:flutter/services.dart';

class CardListScreen extends StatefulWidget {
	@override
	_CardListScreenState createState() => _CardListScreenState();
}

class _CardListScreenState extends State<CardListScreen> {
	List<CardModel> data;
	bool isLoading = false;



	Future getData() async {
		setState(() {
			data = [];
			isLoading = true;
		});

		CardRepository repo = new CardRepository();
		List result = await repo.fetchAll();

		setState(() {
			isLoading = false;
			data = result ?? [];
		});
	}


	@override
	void initState() {
		getData();
		super.initState();
	}

	@override
	Widget build(BuildContext context) {
		return GestureDetector(
			onTap: (){
				FocusScope.of(context).requestFocus(new FocusNode());
				SystemChannels.textInput.invokeMethod('TextInput.hide');
			},
			child: Scaffold(
				appBar: AppBar(
					elevation: 0.0,
					title: Text(
						"Cards",
						textAlign: TextAlign.left,
					),
					actions: <Widget>[
						IconButton(
							icon: Icon(Icons.add),
							onPressed: () async{
								await Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => CardCreateScreen()));
								this.getData();
							},
						)
					],
				),
				body: RefreshIndicator(
					onRefresh: getData,
					child: isLoading
							? Center(
						child: CircularProgressIndicator(),
					)
							: ListView.builder(
									itemCount: data?.length ?? 0,
									itemBuilder: (ctx, index) => Container(
										margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
										child: GestureDetector(
											onTap: () async {

											},
											child: Card(
												shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(12.0)),
												elevation: 1.0,
												child: Container(
													decoration: BoxDecoration(
															borderRadius: BorderRadius.circular(12.0),
															),
													padding: EdgeInsets.all(16.0),
													child: Row(
														mainAxisAlignment: MainAxisAlignment.spaceBetween,
													  children: <Widget>[
													    Column(
													    	crossAxisAlignment: CrossAxisAlignment.start,
													    	children: <Widget>[
													    		Text(
													    			data[index].cardNumber ?? '',
													    			style: TextStyle(fontSize: 24.0, color: Colors.black, fontWeight: FontWeight.w300),
													    		),
													    		Text(
													    			'${data[index].expiryMonth}/${data[index].expiryYear}',
													    			style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.w100),
													    		),
													    	],
													    ),
															IconButton(
																icon: Icon(Icons.clear),
																onPressed: () async {
																	showDialog(
																			context: context,
																			child: AlertDialog(
																				contentPadding: EdgeInsets.all(12),
																				content: Column(
																					mainAxisSize: MainAxisSize.min,
																					crossAxisAlignment: CrossAxisAlignment.end,
																					children: <Widget>[
																						Text('Do you really want to remove card?'),
																						Container(height: 16,),
																						FlatButton(
																							child: Text("Yes", style: TextStyle(color: Colors.blueAccent),),
																							onPressed: () async {
																								await data[index].remove();
																								Navigator.of(context).pop();
																								this.getData();
																							},
																						)
																					],
																				),
																			)
																	);
																},
															)
													  ],
													),
												),
											),
										),
									)),
				),
			),
		);
	}
}
