import 'package:flutter/material.dart';
import 'package:uml_payment_app/models/payment/PaymentRepository.dart';
import 'package:uml_payment_app/models/payment/PaymentModel.dart';
import 'package:uml_payment_app/screens/PaymentSingle.dart';
import 'package:uml_payment_app/screens/Login.dart';
import 'package:uml_payment_app/screens/CardList.dart';
import 'package:uml_payment_app/services/token.service.dart';
import 'package:flutter/services.dart';

class PaymentListScreen extends StatefulWidget {
  @override
  _PaymentListScreenState createState() => _PaymentListScreenState();
}

class _PaymentListScreenState extends State<PaymentListScreen> {
  List<PaymentModel> data;
  bool isLoading = false;
  bool _isFeature = false;

  TextEditingController _searchController = new TextEditingController();

  String _search;


  Future getData() async {
    setState(() {
      data = [];
      isLoading = true;
    });

    PaymentRepository repo = new PaymentRepository();
    List result = await repo.fetchAll();

    setState(() {
      isLoading = false;
      data = result ?? [];
    });
  }

  @override
  void initState() {
    getData();
    _searchController.addListener((){
      setState(() {
        _search = _searchController.text;
      });
    });
    super.initState();
  }

  List get _searchedList => _search != null && _search.isNotEmpty
      ? data.where((PaymentModel model) =>
          model.message.toLowerCase().contains(_search.toLowerCase())
          || model.forward.toLowerCase().contains(_search.toLowerCase())
        ).toList()
      :data;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        FocusScope.of(context).requestFocus(new FocusNode());
        SystemChannels.textInput.invokeMethod('TextInput.hide');
      },
      child: Scaffold(
        drawer: Drawer(
          child: SafeArea(
            child: Column(
              children: <Widget>[
                Text(
                  "UML Payments",
                  style: TextStyle(color: Colors.blueAccent, fontWeight: FontWeight.w300, fontSize: 18),
                ),
                GestureDetector(
                  onLongPress: () {
                    setState(() {
                      _isFeature = true;
                    });
                  },
                  onLongPressUp: () {
                    setState(() {
                      _isFeature = false;
                    });
                  },
                  child: Container(
                    margin: EdgeInsets.only(
                      top: 124,
                    ),
                    width: 45,
                    height: 45,
                    color: Color.fromRGBO(255, 255, 255, 0.1),
                  ),
                ),
                Expanded(
                  child: Container(),
                ),
                ListTile(
                  onTap: () async {
                    Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => CardListScreen()));
                  },
                  leading: Icon(Icons.credit_card),
                  title: Text("Cards"),
                ),
                ListTile(
                  onTap: () async {
                    await TokenService().removeToken();
                    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (ctx) => LoginScreen()));
                  },
                  leading: _isFeature ? Icon(Icons.accessible) : Icon(Icons.first_page),
                  title: Text("Logout"),
                  trailing: _isFeature ? Icon(Icons.accessible_forward) : null,
                )
              ],
            ),
          ),
        ),
        appBar: AppBar(
          elevation: 0.0,
          title: Text(
            "Payments",
            textAlign: TextAlign.left,
          ),
        ),
        body: RefreshIndicator(
          onRefresh: getData,
          child: isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  children: <Widget>[
                    Container(
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                      child: TextField(
                          controller: _searchController,
                          maxLines: 1,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 32.0, top: 16.0, bottom: 16.0),
                            labelText: "Search",
                            labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue),
                                borderRadius: BorderRadius.circular(6.0)
                            ),
                          )
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                          itemCount: _searchedList?.length ?? 0,
                          itemBuilder: (ctx, index) => Container(
                                margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                                child: GestureDetector(
                                  onTap: () async {
                                    await Navigator.of(context).push(MaterialPageRoute(
                                        builder: (ctx) => PaymentSingleScreen(
                                              payment: _searchedList[index],
                                            )));
                                    this.getData();
                                  },
                                  child: Card(
                                    shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(12.0)),
                                    elevation: 1.0,
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(12.0),
                                          border: _searchedList[index].isAuthorized ?? false ? Border.all(color: Colors.green) : Border.all(color: Colors.red)),
                                      padding: EdgeInsets.all(16.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            _searchedList[index].forward ?? '',
                                            style: TextStyle(fontSize: 24.0, color: Colors.black, fontWeight: FontWeight.w300),
                                          ),
                                          Text(
                                            '\$ ${_searchedList[index].amount}',
                                            style: TextStyle(fontSize: 16.0, color: Colors.black, fontWeight: FontWeight.w100),
                                          ),
                                          Container(
                                            height: 1.0,
                                            margin: EdgeInsets.symmetric(vertical: 6),
                                            color: Colors.black12,
                                            width: double.infinity,
                                          ),
                                          Text(
                                            '${_searchedList[index].message}',
                                            style: TextStyle(fontSize: 14.0, color: Colors.black, fontWeight: FontWeight.w100),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )),
                    )
                  ],
                ),
        ),
      ),
    );
  }
}
