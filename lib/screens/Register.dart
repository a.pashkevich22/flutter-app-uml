import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uml_payment_app/services/auth.service.dart';
import 'package:uml_payment_app/screens/PaymentList.dart';


class RegisterScreen extends StatefulWidget {
	@override
	_RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
	GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

	TextEditingController _loginController = new TextEditingController();
	TextEditingController _passwordController = new TextEditingController();
	TextEditingController _nameController = new TextEditingController();
	TextEditingController _surnameController = new TextEditingController();

	String _login;
	String _password;
	String _name;
	String _surname;

	void sendRegistration({String login, String password, String name, String surname}) async {
		AuthService authService = new AuthService();
		bool success = await authService.register(
				login: login,
				password: password,
				name: name,
				surname: surname,
		);
		if(!success)
			_scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Check your data")));
		else
			Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (ctx) => PaymentListScreen()));
	}

	@override
	void initState() {
		super.initState();

		_loginController.addListener((){
			_login = _loginController.text;
		});
		_passwordController.addListener((){
			_password = _passwordController.text;
		});
		_nameController.addListener((){
			_name = _nameController.text;
		});
		_surnameController.addListener((){
			_surname = _surnameController.text;
		});
	}

	@override
	Widget build(BuildContext context) {
		return GestureDetector(
			onTap: (){
				FocusScope.of(context).requestFocus(new FocusNode());
				SystemChannels.textInput.invokeMethod('TextInput.hide');
			},
		  child: Scaffold(
		  	key: _scaffoldKey,
		  	appBar: AppBar(
		  		elevation: 0.0,
		  		title: Text("Registration"),
		  	),
		  	resizeToAvoidBottomPadding: true,
		  	body: Stack(
		  		children: <Widget>[
		  			Positioned.fill(
		  				child: Center(
		  					child: SingleChildScrollView(
		  						child: Container(
		  							padding: EdgeInsets.only(left: 16, right: 16),
		  							child: Column(
		  								crossAxisAlignment: CrossAxisAlignment.start,
		  								mainAxisAlignment: MainAxisAlignment.center,
		  								children: <Widget>[
		  									Container(height: 32,),
		  									TextField(
		  											controller: _nameController,
		  											maxLines: 1,
		  											decoration: InputDecoration(
		  												contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
		  												labelText: "Name",
		  												labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
		  												enabledBorder: OutlineInputBorder(
		  													borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
		  													borderRadius: BorderRadius.circular(6.0),
		  												),
		  												focusedBorder: OutlineInputBorder(
		  														borderSide: BorderSide(color: Colors.blue),
		  														borderRadius: BorderRadius.circular(6.0)
		  												),
		  											)
		  									),
		  									Container(height: 24),
		  									TextField(
		  											controller: _surnameController,
		  											maxLines: 1,
		  											decoration: InputDecoration(
		  												contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
		  												labelText: "Surname",
		  												labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
		  												enabledBorder: OutlineInputBorder(
		  													borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
		  													borderRadius: BorderRadius.circular(6.0),
		  												),
		  												focusedBorder: OutlineInputBorder(
		  														borderSide: BorderSide(color: Colors.blue),
		  														borderRadius: BorderRadius.circular(6.0)
		  												),
		  											)
		  									),
		  									Container(height: 24),
		  									TextField(
		  											controller: _loginController,
		  											maxLines: 1,
		  											decoration: InputDecoration(
		  												contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
		  												labelText: "Login",
		  												labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
		  												enabledBorder: OutlineInputBorder(
		  													borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
		  													borderRadius: BorderRadius.circular(6.0),
		  												),
		  												focusedBorder: OutlineInputBorder(
		  														borderSide: BorderSide(color: Colors.blue),
		  														borderRadius: BorderRadius.circular(6.0)
		  												),
		  											)
		  									),
		  									Container(height: 24),
		  									TextField(
		  											controller: _passwordController,
		  											maxLines: 1,
		  											obscureText: true,
		  											decoration: InputDecoration(
		  												contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
		  												labelText: "Password",
		  												labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
		  												enabledBorder: OutlineInputBorder(
		  													borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
		  													borderRadius: BorderRadius.circular(6.0),
		  												),
		  												focusedBorder: OutlineInputBorder(
		  														borderSide: BorderSide(color: Colors.blue),
		  														borderRadius: BorderRadius.circular(6.0)
		  												),
		  											)
		  									),
		  									Container(
		  										width: double.infinity,
		  										height: 56.0,
		  										margin: EdgeInsets.symmetric(vertical: 24.0),
		  										child: RaisedButton(
		  											onPressed: () => sendRegistration(
		  													login: _login,
		  													password: _password,
		  													name: _name,
		  													surname: _surname
		  											),
		  											elevation: 1.0,
		  											color: Colors.blueAccent,
		  											child: Text("Register",
		  												style: TextStyle(
		  														fontWeight: FontWeight.w300,
		  														color: Colors.white
		  												),
		  											),
		  										),
		  									)
		  								],
		  							),
		  						),
		  					),
		  				),
		  			),
		  		],
		  	),
		  ),
		);
	}
}
