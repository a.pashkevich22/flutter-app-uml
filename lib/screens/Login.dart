import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uml_payment_app/services/auth.service.dart';
import 'package:uml_payment_app/screens/PaymentList.dart';
import 'package:uml_payment_app/screens/Register.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
	GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

	TextEditingController _loginController = new TextEditingController();
	TextEditingController _passwordController = new TextEditingController();

	String _login;
	String _password;

	void sendAuth({String login, String password}) async {
		AuthService authService = new AuthService();
		bool success = await authService.login(login: login, password: password);
		if(!success)
			_scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Invalid login/password")));
		else
			Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (ctx) => PaymentListScreen()));
	}

	@override
  void initState() {
    super.initState();

    _loginController.addListener((){
    	_login = _loginController.text;
		});
    _passwordController.addListener((){
    	_password = _passwordController.text;
		});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
			key: _scaffoldKey,
			appBar: AppBar(
				elevation: 0.0,
				title: Text("Authorization"),
			),
			resizeToAvoidBottomPadding: true,
			body: GestureDetector(
				onTap: (){
					FocusScope.of(context).requestFocus(new FocusNode());
					SystemChannels.textInput.invokeMethod('TextInput.hide');
				},
			  child: Stack(
			    children: <Widget>[
			      Positioned.fill(
			        child: Center(
			          child: SingleChildScrollView(
			            child: Container(
			            	padding: EdgeInsets.only(left: 16, right: 16),
			              child: Column(
			  							crossAxisAlignment: CrossAxisAlignment.start,
			  							mainAxisAlignment: MainAxisAlignment.center,
			            		children: <Widget>[
			            			Text("UML Payments",
			  									style: TextStyle(
			  											fontSize: 24.0,
			  											color: Colors.black,
			  											fontWeight: FontWeight.w300
			  									),
			  								),
			  								Text("App for secure payments",
			  									style: TextStyle(
			  											fontSize: 16.0,
			  											color: Colors.black,
			  											fontWeight: FontWeight.w100
			  									),
			  								),
			  								Container(height: 24),
			  								TextField(
														controller: _loginController,
			            					maxLines: 1,
			            					decoration: InputDecoration(
			            						contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
			            						labelText: "Login",
			            						labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
			            						enabledBorder: OutlineInputBorder(
			            							borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
			            							borderRadius: BorderRadius.circular(6.0),
			            						),
			            						focusedBorder: OutlineInputBorder(
			            								borderSide: BorderSide(color: Colors.blue),
			            								borderRadius: BorderRadius.circular(6.0)
			            						),
			            					)
			            				),
			            				Container(height: 24),
			            				TextField(
														controller: _passwordController,
			            					maxLines: 1,
			            					obscureText: true,
			            					decoration: InputDecoration(
			            						contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
			            						labelText: "Password",
			            						labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
			            						enabledBorder: OutlineInputBorder(
			            							borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
			            							borderRadius: BorderRadius.circular(6.0),
			            						),
			            						focusedBorder: OutlineInputBorder(
			            								borderSide: BorderSide(color: Colors.blue),
			            								borderRadius: BorderRadius.circular(6.0)
			            						),
			            					)
			            				),
			  									Container(
			  										width: double.infinity,
			  									  height: 56.0,
			  									  margin: EdgeInsets.symmetric(vertical: 24.0),
			  									  child: RaisedButton(
			  									  	onPressed: () => sendAuth(login: _login, password: _password),
			  									  	elevation: 1.0,
			  									  	color: Colors.blueAccent,
			  									  	child: Text("Sign in",
			  												style: TextStyle(
			  													fontWeight: FontWeight.w300,
			  													color: Colors.white
			  												),
			  											),
			  									  ),
			  									),
												GestureDetector(
													onTap: (){
														Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => RegisterScreen()));
													},
													child: Container(
														padding: EdgeInsets.symmetric(vertical: 6),
														child: Text('Register'),
													),
												)
			            		],
			              ),
			            ),
			          ),
			        ),
			      ),
			    ],
			  ),
			),
		);
  }
}
