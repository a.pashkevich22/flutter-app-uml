import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:uml_payment_app/models/card/CardModel.dart';


class CardCreateScreen extends StatefulWidget {
	@override
	_CardCreateScreenState createState() => _CardCreateScreenState();
}

class _CardCreateScreenState extends State<CardCreateScreen> {
	GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

	TextEditingController _cardNumberController = new MaskedTextController(mask: '0000 0000 0000 0000');
	TextEditingController _cvvController = new MaskedTextController(mask: '000');
	TextEditingController _expiryMonthController = new MaskedTextController(mask: '00');
	TextEditingController _expiryYearController = new MaskedTextController(mask: '0000');

	String _expiryMonth;
	String _cardNumber;
	String _expiryYear;
	String _cvv;

	void createCard({String expiryMonth, String expityYear, String cardNumber, String cvv}) async {
		CardModel cardModel = new CardModel(
			expiryMonth: expiryMonth,
			expiryYear: expityYear,
			cvv: cvv,
			cardNumber: cardNumber
		);
		bool success = await cardModel.create();
		if(!success)
			_scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Check your data")));
		else
			Navigator.of(context).pop();
	}

	@override
	void initState() {
		super.initState();

		_cardNumberController.addListener((){
			_cardNumber = _cardNumberController.text;
		});
		_cvvController.addListener((){
			_cvv = _cvvController.text;
		});
		_expiryMonthController.addListener((){
			_expiryMonth = _expiryMonthController.text;
		});
		_expiryYearController.addListener((){
			_expiryYear = _expiryYearController.text;
		});
	}

	@override
	Widget build(BuildContext context) {
		return GestureDetector(
			onTap:(){
				FocusScope.of(context).requestFocus(new FocusNode());
				SystemChannels.textInput.invokeMethod('TextInput.hide');
			},
		  child: Scaffold(
		  	key: _scaffoldKey,
		  	appBar: AppBar(
		  		elevation: 0.0,
		  		title: Text("Add card"),
		  	),
		  	resizeToAvoidBottomPadding: true,
		  	body: Stack(
		  		children: <Widget>[
		  			Positioned.fill(
		  				child: Center(
		  					child: SingleChildScrollView(
		  						child: Container(
		  							padding: EdgeInsets.only(left: 16, right: 16),
		  							child: Column(
		  								crossAxisAlignment: CrossAxisAlignment.start,
		  								mainAxisAlignment: MainAxisAlignment.center,
		  								children: <Widget>[
		  									Container(height: 32,),
		  									TextField(
		  											controller: _cardNumberController,
		  											maxLines: 1,
		  											decoration: InputDecoration(
		  												contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
		  												labelText: "Card Number",
		  												labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
		  												enabledBorder: OutlineInputBorder(
		  													borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
		  													borderRadius: BorderRadius.circular(6.0),
		  												),
		  												focusedBorder: OutlineInputBorder(
		  														borderSide: BorderSide(color: Colors.blue),
		  														borderRadius: BorderRadius.circular(6.0)
		  												),
		  											)
		  									),
		  									Container(height: 24),
		  									TextField(
		  											controller: _expiryYearController,
		  											maxLines: 1,
		  											decoration: InputDecoration(
		  												contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
		  												labelText: "Year",
		  												labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
		  												enabledBorder: OutlineInputBorder(
		  													borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
		  													borderRadius: BorderRadius.circular(6.0),
		  												),
		  												focusedBorder: OutlineInputBorder(
		  														borderSide: BorderSide(color: Colors.blue),
		  														borderRadius: BorderRadius.circular(6.0)
		  												),
		  											)
		  									),
		  									Container(height: 24),
		  									TextField(
		  											controller: _expiryMonthController,
		  											maxLines: 1,
		  											decoration: InputDecoration(
		  												contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
		  												labelText: "Month",
		  												labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
		  												enabledBorder: OutlineInputBorder(
		  													borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
		  													borderRadius: BorderRadius.circular(6.0),
		  												),
		  												focusedBorder: OutlineInputBorder(
		  														borderSide: BorderSide(color: Colors.blue),
		  														borderRadius: BorderRadius.circular(6.0)
		  												),
		  											)
		  									),
		  									Container(height: 24),
		  									TextField(
		  											controller: _cvvController,
		  											maxLines: 1,
		  											obscureText: true,
		  											decoration: InputDecoration(
		  												contentPadding: EdgeInsets.only(left: 32.0, top: 22.0, bottom: 22.0),
		  												labelText: "CVV",
		  												labelStyle: TextStyle(color: Color(0xff6f6f6f), fontSize: 17.0), //#6f6f6f
		  												enabledBorder: OutlineInputBorder(
		  													borderSide: BorderSide(color: Color(0xffe0e0e0)), // #e0e0e0
		  													borderRadius: BorderRadius.circular(6.0),
		  												),
		  												focusedBorder: OutlineInputBorder(
		  														borderSide: BorderSide(color: Colors.blue),
		  														borderRadius: BorderRadius.circular(6.0)
		  												),
		  											)
		  									),
		  									Container(
		  										width: double.infinity,
		  										height: 56.0,
		  										margin: EdgeInsets.symmetric(vertical: 24.0),
		  										child: RaisedButton(
		  											onPressed: () => createCard(
		  													expityYear: _expiryYear,
		  													expiryMonth: _expiryMonth,
		  													cardNumber: _cardNumber,
		  													cvv: _cvv
		  											),
		  											elevation: 1.0,
		  											color: Colors.blueAccent,
		  											child: Text("Add",
		  												style: TextStyle(
		  														fontWeight: FontWeight.w300,
		  														color: Colors.white
		  												),
		  											),
		  										),
		  									)
		  								],
		  							),
		  						),
		  					),
		  				),
		  			),
		  		],
		  	),
		  ),
		);
	}
}
