import 'package:shared_preferences/shared_preferences.dart';

class TokenService {
	Future<SharedPreferences> _getSharedPrefs() async{
		SharedPreferences _sharedPreferences =  await SharedPreferences.getInstance();
		return _sharedPreferences;
	}

	Future saveToken(String token) async{
		(await _getSharedPrefs()).setString("token", token);
	}

	Future<String> getToken() async{
		return (await _getSharedPrefs()).getString("token");
	}

	Future removeToken() async{
		(await _getSharedPrefs()).remove("token");
	}
}