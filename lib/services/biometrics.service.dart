import 'package:local_auth/local_auth.dart';


class BiometricsService {
	Future<bool> canCheckBiometrics() async {
		LocalAuthentication la = new LocalAuthentication();
		bool _canCheckBiometrics = await la.canCheckBiometrics;
		return _canCheckBiometrics;
	}

	Future<bool> authorize(String message) async{
		LocalAuthentication la = new LocalAuthentication();
		bool isAuthorized = await la.authenticateWithBiometrics(localizedReason: message);
		return isAuthorized;
	}
}