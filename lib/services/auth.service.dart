import 'package:dio/dio.dart';
import 'package:uml_payment_app/constants.dart';
import 'package:uml_payment_app/services/token.service.dart';
Dio _http = new Dio();


class AuthService {

	Future<bool> login({String login, String password}) async {
		if(login == null || password == null)
			return false;

		try {
			Response response = await _http.post('${Constant.serverUrl}/auth/login', data: { "login": login, "password": password });

			if(response.data['ok'] && response.data['token'] != null) {
				TokenService tkn = new TokenService();
				await tkn.saveToken(response.data['token']);
				return true;

			} else
				return false;

		} on DioError catch(e) {
			print(e);
			return false;
		}
	}


	Future<bool> register({String login, String password, String name, String surname}) async {
		if(login == null || password == null || name == null || surname == null)
			return false;

		try {
			Response response = await _http.post('${Constant.serverUrl}/auth/register',
					data: {
						"login": login,
						"password": password,
						"name": name,
						"surname": surname,
					}
			);

			if(response.data['ok'] && response.data['token'] != null) {
				TokenService tkn = new TokenService();
				await tkn.saveToken(response.data['token']);
				return true;

			} else
				return false;

		} on DioError catch(e) {
			print(e);
			return false;
		}
	}
}